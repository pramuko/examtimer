# About ExamTimer

ExamTimer is a timer app designed to be used during class exams.
ExamTimer has the following characteristics:

- This app is designed to be displayed through a projector.
- Since its purpose is to be displayed for other people in a classroom,
  the configuration function and timer display is split into two windows.
- This app will detect if the PC is connected to an external projector.
- The timer only shows hours and minutes, no seconds nor milliseconds.

# Screenshots
These screenshots show how ExamTimer would appear when run on a dual display 
systems (i.e. a laptop attached to a projector). The first half is the default 
monitor, the second half is the projector or second monitor.

### Ubuntu
![ExamTimer appearance on a dual monitor Ubuntu](http://pramuko.staff.telkomuniversity.ac.id/files/2015/04/ExamTimer-on-dual-monitor-system-25.jpg)

### Windows 8.1
![ExamTimer appearance on a dual monitor Windows 8.1]
(http://pramuko.staff.telkomuniversity.ac.id/files/2015/04/ExamTimer-on-dual-monitor-system-windows-8.1-25.jpg)

# System requirements
- Windows, Mac, or Linux
- [Java SE 8](http://www.oracle.com/technetwork/java/javase/downloads/index.html) or later