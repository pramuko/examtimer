package pret;

import javafx.animation.*;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.geometry.Rectangle2D;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Region;
import javafx.scene.paint.Color;
import javafx.scene.shape.Arc;
import javafx.scene.shape.ArcType;
import javafx.scene.shape.StrokeLineCap;
import javafx.scene.shape.StrokeType;
import javafx.scene.text.Font;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;

import java.util.ResourceBundle;

/**
 * Stage for displaying the remaining time.
 */
class TimerStage extends Stage {
    private DisplayMode mode = DisplayMode.TEXT;

    private double xOffset = 0;
    private double yOffset = 0;
    private Pane pane;
    private Label remainingHoursText, remainingMinutesText, colonText;
    private Arc arc, elapsedArc;
    private Color arcColor = Color.YELLOWGREEN;
    private Color elapsedArcColor = Color.DARKRED;
    private Color normalTextColor = Color.BLACK;

    // Node height and width are calculated based on percentage to stage height.
    // Designed with my laptop which has 768 px height.
    private final static double TEXT_ONLY_HEIGHT = 0.4;
    private final static double TEXT_IN_ARC_HEIGHT = 0.3;
    private final static double ARC_RADIUS = 0.39;          // 600.0 / 768
    private final static double ARC_STROKE_WIDTH = 0.09;    // 69 / 768

    private final static double PX_TO_PTS = 0.75;
    private SequentialTransition blinkTransition;

    public TimerStage() {
        super();

        ResourceBundle rs = ResourceBundle.getBundle("pret.assets.bundles.UiLang");

        initStyle(StageStyle.DECORATED);
        setTitle(String.format("%s - %s",
                rs.getString("remainingTime"),
                rs.getString("appName")));
        pane = new Pane();
        setScene(new Scene(pane, 600, 400));
        initIndicators();

        setupMouseDrag(pane);
        SizeListener listener = new SizeListener();
        widthProperty().addListener(listener);
        heightProperty().addListener(listener);
    }

    public void setMode(DisplayMode mode) {
        this.mode = mode;
    }

    private void initIndicators() {
        arc = new Arc(getWidth() / 2, getHeight() / 2, 300, 300, 90, 360);
        arc.setStrokeWidth(69);
        arc.setStrokeMiterLimit(0);
        arc.setType(ArcType.OPEN);
        arc.setStroke(arcColor);
        arc.setStrokeType(StrokeType.CENTERED);
        arc.setStrokeLineCap(StrokeLineCap.BUTT);
        arc.setFill(null);

        elapsedArc = new Arc(getWidth() / 2, getHeight() / 2, 300, 300, 90, 360);
        elapsedArc.setStrokeWidth(69);
        elapsedArc.setStrokeMiterLimit(0);
        elapsedArc.setType(ArcType.OPEN);
        elapsedArc.setStroke(elapsedArcColor);
        elapsedArc.setStrokeType(StrokeType.CENTERED);
        elapsedArc.setStrokeLineCap(StrokeLineCap.BUTT);
        elapsedArc.setFill(null);

        remainingHoursText = new Label("00");
        colonText = new Label(":");
        remainingMinutesText = new Label("00");
        HBox textBox = new HBox();
        textBox.setPrefWidth(Region.USE_COMPUTED_SIZE);
        textBox.setPrefHeight(Region.USE_COMPUTED_SIZE);
        textBox.getChildren().addAll(remainingHoursText, colonText, remainingMinutesText);

        pane.getChildren().addAll(elapsedArc, arc, textBox);
    }

    public void repositionIndicators() {
        double centerX = getWidth() / 2;
        double centerY = getHeight() / 2;

        if (mode == DisplayMode.TEXT_WITHIN_ARC) {
            double stroke = ARC_STROKE_WIDTH * getHeight();
            double radius = ARC_RADIUS * getHeight();

            arc.setStroke(arcColor);
            arc.setStrokeWidth(stroke);
            arc.setRadiusX(radius);
            arc.setRadiusY(radius);
            arc.setCenterX(centerX);
            arc.setCenterY(centerY);

            elapsedArc.setStroke(elapsedArcColor);
            elapsedArc.setStrokeWidth(stroke);
            elapsedArc.setRadiusX(radius);
            elapsedArc.setRadiusY(radius);
            elapsedArc.setCenterX(centerX);
            elapsedArc.setCenterY(centerY);

        } else {
            arc.setStrokeWidth(0);
            elapsedArc.setStrokeWidth(0);
        }

        Font calibri = new Font("Calibri", calculateTextSize(mode));
        remainingHoursText.setFont(calibri);
        colonText.setFont(calibri);
        remainingMinutesText.setFont(calibri);

        Parent parent = remainingHoursText.getParent();
        parent.autosize();
        parent.relocate(centerX - (remainingHoursText.getWidth() +
                        colonText.getWidth() +
                        remainingMinutesText.getWidth()) / 2,
                centerY - (remainingHoursText.getHeight()) / 2);

        System.err.println(String.format("Stage: %f, %f", getWidth(), getHeight()));
        System.err.println(String.format("Text position: %f, %f\n",
                remainingHoursText.getParent().getLayoutX(),
                remainingHoursText.getParent().getLayoutY()));
    }

    private int calculateTextSize(DisplayMode mode) {
        if (mode == DisplayMode.TEXT) {
            return (int) (getHeight() * TEXT_ONLY_HEIGHT * PX_TO_PTS);
        }

        return (int) (getHeight() * TEXT_IN_ARC_HEIGHT * PX_TO_PTS);
    }

    public void initTransitions() {
        Timeline blinker = createBlinker(colonText);
        blinkTransition = new SequentialTransition(blinker);
        blinkTransition.play();
    }

    public void stopTransitions() {
        blinkTransition.stop();
    }

    public void update(int remaining, int total) {
        arc.setLength(360.0 * remaining / total);
        elapsedArc.setLength(360.0 * (total - remaining) / total);
        elapsedArc.setStartAngle(90 - 360.0 * (total - remaining) / total);

        int[] timeSegments = secondsToHoursMinutes(remaining);
        remainingHoursText.setText(String.format("%02d", timeSegments[0]));
        remainingMinutesText.setText(String.format("%02d", timeSegments[1]));

        if (remaining == 0) {
            stopTransitions();
            arc.setLength(0);
            elapsedArc.setLength(360);
            remainingHoursText.setTextFill(elapsedArcColor);
            colonText.setTextFill(elapsedArcColor);
            remainingMinutesText.setTextFill(elapsedArcColor);
            remainingHoursText.setOpacity(1);
            colonText.setOpacity(1);
            remainingMinutesText.setOpacity(1);
        } else {
            remainingHoursText.setTextFill(normalTextColor);
            colonText.setTextFill(normalTextColor);
            remainingMinutesText.setTextFill(normalTextColor);
        }

        // FIXME: arcs can be (rarely) stuck for a few seconds. Need to double check current length vs expected length
        // and adjust if there is any discrepancy.
    }

    /**
     * Search for screens for placing the stage. If there is only one screen,
     * display it there. Otherwise, display it on the second one.
     */
    public Screen determineTargetScreen() {
        ObservableList<Screen> screens = Screen.getScreens();
        Screen targetScreen;
        if (screens.size() > 1) {
            targetScreen = screens.get(1);
            setFullScreen(true);
        } else {
            targetScreen = Screen.getPrimary();
            setFullScreen(false);
        }

        return targetScreen;
    }

    /**
     * Move this stage to target screen. Useful for i.e. when user disconnect
     * the secondary screen.
     */
    public void moveToTargetScreen(Screen screen) {
        Rectangle2D screenBounds = screen.getVisualBounds();
        setX(screenBounds.getMinX());
        setY(screenBounds.getMinY());
        setWidth(screenBounds.getWidth());
        setHeight(screenBounds.getHeight());
    }

    private void setupMouseDrag(Pane pane1) {
        pane1.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                xOffset = getX() - event.getScreenX();
                yOffset = getY() - event.getScreenY();
            }
        });

        pane1.setOnMouseDragged(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                setX(event.getScreenX() + xOffset);
                setY(event.getScreenY() + yOffset);
            }
        });
    }

    public void setElapsedArcColor(Color elapsedArcColor) {
        this.elapsedArcColor = elapsedArcColor;
    }

    public void setArcColor(Color arcColor) {
        this.arcColor = arcColor;
    }

    /**
     * Converts duration to an array of ints consists of hours, minutes, seconds.
     * @param duration in seconds
     * @return array of int consists of hours, minutes, and seconds
     */
    public static int[] secondsToHoursMinutesSeconds(int duration) {
        int hours = duration / 3600;
        int minutes = (duration - hours * 3600) % 3600 / 60;
        int seconds = duration - hours * 3600 - minutes * 60;
        return new int[]{hours, minutes, seconds};
    }

    /**
     * Converts seconds to hours and minutes.
     * @param duration
     * @return array of int consists of hours and minutes
     */
    public static int[] secondsToHoursMinutes(int duration) {
        int[] hms = secondsToHoursMinutesSeconds(duration);
        int h, m;
        if (hms[2] > 0 && hms[1] != 59) {
            h = hms[0];
            m = hms[1] + 1;
        } else if (hms[2] > 0 && hms[1] == 59) {
            h = hms[0] + 1;
            m = 0;
        } else {
            h = hms[0];
            m = hms[1];
        }
        return new int[]{h, m};
    }

    private Timeline createBlinker(Node node) {
        Timeline blink = new Timeline(
                new KeyFrame(
                        Duration.seconds(0),
                        new KeyValue(
                                node.opacityProperty(),
                                1,
                                Interpolator.DISCRETE
                        )
                ),
                new KeyFrame(Duration.seconds(0.4),
                        new KeyValue(
                                node.opacityProperty(),
                                1,
                                Interpolator.DISCRETE
                        )
                ),
                new KeyFrame(
                        Duration.seconds(0.5),
                        new KeyValue(
                                node.opacityProperty(),
                                0,
                                Interpolator.EASE_IN
                        )
                ),
                new KeyFrame(Duration.seconds(0.9),
                        new KeyValue(
                                node.opacityProperty(),
                                0,
                                Interpolator.DISCRETE
                        )
                ),
                new KeyFrame(
                        Duration.seconds(1),
                        new KeyValue(
                                node.opacityProperty(),
                                1,
                                Interpolator.EASE_IN
                        )
                )
        );
        blink.setCycleCount(Animation.INDEFINITE);

        return blink;
    }


    class SizeListener implements ChangeListener<Number> {

        @Override
        public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
            if (getWidth() > 1 && getHeight() > 1) {
                repositionIndicators();

            } else {
                final Runnable r = new Runnable() {
                    @Override
                    public void run() {
                        repositionIndicators();
                    }
                };

                Thread t = new Thread() {
                    @Override
                    public void run() {
                        try {
                            Thread.sleep(50);
                            Platform.runLater(r);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                };
                t.start();
            }
        }
    }
}
