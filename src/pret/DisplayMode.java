package pret;

/**
 * This enum lists modes available when the timer is running.
 */
public enum DisplayMode {
    // Display only the remaining time
    TEXT,

    // Display the remaining time inside an arc
    TEXT_WITHIN_ARC
}
