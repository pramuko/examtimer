package pret;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.*;

public class ConfigControllerTest {

    @Test
    public void testCalculateDuration() throws Exception {
        ConfigController instance = new ConfigController();
        assertEquals(5400, instance.calculateDuration(1, 30));
        assertEquals(2700, instance.calculateDuration(0, 45));
        assertEquals(21600, instance.calculateDuration(6, 0));
        assertEquals(0, instance.calculateDuration(0, 0));
    }

}