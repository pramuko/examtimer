/*
 * License: BSD
 */

package pret;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.ResourceBundle;
import java.util.Timer;
import java.util.TimerTask;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.paint.Color;
import javafx.stage.WindowEvent;

/**
 *
 * @author Pramuko Aji
 */
public class ConfigController implements Initializable {
    private ResourceBundle rb;

    private int totalSeconds;
    private boolean isRunning = false;
    private Timer timer;
    private UpdateTimerTask updateTimerTask;

    @FXML
    private ChoiceBox<Integer> hoursChoice;

    @FXML
    private ComboBox<Integer> minutesCombo;
    
    @FXML
    private ChoiceBox<String> modeChoice;

    @FXML
    private ColorPicker elapsedArcColor;

    @FXML
    private ColorPicker remainingArcColor;

    @FXML
    private Button button;

    @FXML
    private Label startTimeStatus;

    @FXML
    private Label remainingTimeStatus;

    private TimerStage timerStage;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        this.rb = rb;

        elapsedArcColor.setValue(Color.FIREBRICK);
        remainingArcColor.setValue(Color.YELLOWGREEN);
        hoursChoice.setValue(1);
        minutesCombo.setValue(30);
        modeChoice.getItems().addAll(rb.getString("mode0"), rb.getString("mode1"));
        modeChoice.setValue(rb.getString("mode1"));
    }

    @FXML
    private void handleButtonAction(ActionEvent event) {
        if (!isRunning) {
            if (hoursChoice.getValue() == 0 && minutesCombo.getValue() == 0) {
                return;
            }

            startCounting();
            showDisplay();
            timerStage.initTransitions();
        } else {
            timerStage.stopTransitions();
            closeDisplay();
            stopCounting();
        }
    }

    private void startCounting() {
        totalSeconds = calculateDuration(hoursChoice.getValue(), minutesCombo.getValue());

        updateTimerTask = new UpdateTimerTask(totalSeconds);
        timer = new Timer();
        timer.scheduleAtFixedRate(updateTimerTask, 1000, 1000);

        isRunning = true;

        Calendar cal = Calendar.getInstance();
        cal.getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        System.out.println(sdf.format(cal.getTime()));
        startTimeStatus.setText(String.format("%s: %s",
                rb.getString("started"),
                sdf.format(cal.getTime())));
    }

    public void stopCounting() {
        if (updateTimerTask != null) {
            updateTimerTask.cancel();
        }

        if (timer != null) {
            timer.cancel();
            timer.purge();
        }

        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                button.setText(rb.getString("start"));
                startTimeStatus.setText(null);
                remainingTimeStatus.setText(null);
            }
        });

        isRunning = false;
    }

    public void showDisplay() {
        if (timerStage == null) {
            timerStage = new TimerStage();
            timerStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
                public void handle(WindowEvent we) {
                    Platform.runLater(new Runnable() {
                        @Override
                        public void run() {
                            stopCounting();
                        }
                    });
                }
            });
        }

        if (modeChoice.getValue().contentEquals(rb.getString("mode0"))) {
            timerStage.setMode(DisplayMode.TEXT);
        } else {
            timerStage.setMode(DisplayMode.TEXT_WITHIN_ARC);
        }

        timerStage.setElapsedArcColor(elapsedArcColor.getValue());
        timerStage.setArcColor(remainingArcColor.getValue());

        timerStage.update(totalSeconds, totalSeconds);
        timerStage.moveToTargetScreen(timerStage.determineTargetScreen());
        timerStage.show();
        timerStage.repositionIndicators();

        final Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(300);
                    System.err.println("Slept for 300 ms before reposition.");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        timerStage.repositionIndicators();
                    }
                });
            }
        });
        t.start();

        button.setText(rb.getString("cancel"));
    }

    public void closeDisplay() {
        if (timerStage != null && timerStage.isShowing()) {
            Platform.runLater(new Runnable() {
                @Override
                public void run() {
                    timerStage.close();
                }
            });
        }
    }

    int calculateDuration(int hours, int minutes) {
        return hours * 60 * 60 + minutes * 60;
    }


    class UpdateTimerTask extends TimerTask {
        private int secondsLeft;

        protected UpdateTimerTask(int totalSeconds) {
            super();
            secondsLeft = totalSeconds;
        }

        @Override
        public void run() {
            if (secondsLeft > 0) {
                secondsLeft -= 1;
            } else {
                stopCounting();
            }

            Platform.runLater(new Runnable() {
                @Override
                public void run() {
                    remainingTimeStatus.setText(
                            String.format("%s: %02d:%02d:%02d",
                                    rb.getString("timeLeft"),
                                    TimerStage.secondsToHoursMinutesSeconds(secondsLeft)[0],
                                    TimerStage.secondsToHoursMinutesSeconds(secondsLeft)[1],
                                    TimerStage.secondsToHoursMinutesSeconds(secondsLeft)[2]));
                    timerStage.update(secondsLeft, totalSeconds);
                }
            });
        }
    }

}
