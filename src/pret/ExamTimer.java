/*
 * License: BSD
 */

package pret;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import java.util.ResourceBundle;

/**
 * PrET (Pram's Exam Timer)
 * @author Pramuko Aji
 */
public class ExamTimer extends Application {
    
    @Override
    public void start(Stage stage) throws Exception {
        final FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("assets/config.fxml"));
        ResourceBundle rb = ResourceBundle.getBundle("pret.assets.bundles.UiLang");
        fxmlLoader.setResources(rb);
        final ConfigController configController = new ConfigController();
        fxmlLoader.setController(configController);
        Parent root = fxmlLoader.load();

        Scene scene = new Scene(root);

        stage.setTitle(rb.getString("appName"));
        stage.setScene(scene);
        stage.show();

        stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            public void handle(WindowEvent we) {
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        configController.stopCounting();
                        configController.closeDisplay();
                        Platform.exit();
                    }
                });
            }
        });
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}
