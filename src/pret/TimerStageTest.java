package pret;

import org.junit.Test;

import static org.junit.Assert.*;

public class TimerStageTest {

    @Test
    public void testHoursMinutesSeconds() throws Exception {
        int seconds = 1860;
        assertEquals(0, TimerStage.secondsToHoursMinutesSeconds(seconds)[0]);
        assertEquals(31, TimerStage.secondsToHoursMinutesSeconds(seconds)[1]);
        assertEquals(0, TimerStage.secondsToHoursMinutesSeconds(seconds)[2]);

        seconds = 422;
        assertEquals(0, TimerStage.secondsToHoursMinutesSeconds(seconds)[0]);
        assertEquals(7, TimerStage.secondsToHoursMinutesSeconds(seconds)[1]);
        assertEquals(2, TimerStage.secondsToHoursMinutesSeconds(seconds)[2]);

        seconds = 10020;
        assertEquals(2, TimerStage.secondsToHoursMinutesSeconds(seconds)[0]);
        assertEquals(47, TimerStage.secondsToHoursMinutesSeconds(seconds)[1]);
        assertEquals(0, TimerStage.secondsToHoursMinutesSeconds(seconds)[2]);

        seconds = 3600;
        assertEquals(1, TimerStage.secondsToHoursMinutesSeconds(seconds)[0]);
        assertEquals(0, TimerStage.secondsToHoursMinutesSeconds(seconds)[1]);
        assertEquals(0, TimerStage.secondsToHoursMinutesSeconds(seconds)[2]);

        seconds = 3540;
        assertEquals(0, TimerStage.secondsToHoursMinutesSeconds(seconds)[0]);
        assertEquals(59, TimerStage.secondsToHoursMinutesSeconds(seconds)[1]);
        assertEquals(0, TimerStage.secondsToHoursMinutesSeconds(seconds)[2]);

        seconds = 0;
        assertEquals(0, TimerStage.secondsToHoursMinutesSeconds(seconds)[0]);
        assertEquals(0, TimerStage.secondsToHoursMinutesSeconds(seconds)[1]);
        assertEquals(0, TimerStage.secondsToHoursMinutesSeconds(seconds)[2]);
    }

    @Test
    public void testHoursMinutes() throws Exception {
        int seconds = 0;
        assertEquals(0, TimerStage.secondsToHoursMinutes(seconds)[0]);
        assertEquals(0, TimerStage.secondsToHoursMinutes(seconds)[1]);

        seconds = 60;
        assertEquals(0, TimerStage.secondsToHoursMinutes(seconds)[0]);
        assertEquals(1, TimerStage.secondsToHoursMinutes(seconds)[1]);

        seconds = 3599;
        assertEquals(1, TimerStage.secondsToHoursMinutes(seconds)[0]);
        assertEquals(0, TimerStage.secondsToHoursMinutes(seconds)[1]);

        seconds = 3600;
        assertEquals(1, TimerStage.secondsToHoursMinutes(seconds)[0]);
        assertEquals(0, TimerStage.secondsToHoursMinutes(seconds)[1]);

        seconds = 3601;
        assertEquals(1, TimerStage.secondsToHoursMinutes(seconds)[0]);
        assertEquals(1, TimerStage.secondsToHoursMinutes(seconds)[1]);

    }
}